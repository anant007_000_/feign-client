package customer.bill.services.entity;

import javax.annotation.Generated;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nonapi.io.github.classgraph.json.Id;

@Data
@Getter
@Setter

@NoArgsConstructor
public class Bill {
	
	
	@NotNull(message = "Please enter valid id")
	private long billId;
	@Max(value = 10)
	@Min(value=2)
	@Positive(message = "totalDays value cannot be negative")
	private int totalDays;

	
	@Positive(message = "totalDays value cannot be negative")
		private int totalCost;
	
	
	private long customerId;

}
